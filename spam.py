import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import StratifiedKFold,cross_val_score,cross_validate
from sklearn.metrics import confusion_matrix,make_scorer
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB
import re
import matplotlib.pyplot as plt
from matplotlib import gridspec
import itertools
import matplotlib.colors as mcolors

#%store -r r

for k in r.keys():
    r[k]['cm'] = np.array([[r[k]['test_tn'].sum(), r[k]['test_fp'].sum()], [r[k]['test_fn'].sum(), r[k]['test_tp'].sum()]])
    r[k]['prec'] = r[k]['test_tp'].sum()/(r[k]['test_tp'].sum()+r[k]['test_fp'].sum())
    r[k]['acc'] = (r[k]['test_tp'].sum()+r[k]['test_tn'].sum())/(r[k]['test_tp'].sum()+r[k]['test_tn'].sum()+r[k]['test_fn'].sum()+r[k]['test_fp'].sum())
    r[k]['esp'] = r[k]['test_tn'].sum()/(r[k]['test_tn'].sum()+r[k]['test_fp'].sum())
    r[k]['rec'] = r[k]['test_tp'].sum()/(r[k]['test_tp'].sum()+r[k]['test_fn'].sum())
    r[k]['f'] = 2/((1/r[k]['prec'])+(1/r[k]['rec']))
    r[k]['t1'] = r[k]['test_fp'].sum()/(r[k]['test_fp'].sum()+r[k]['test_tn'].sum())
    r[k]['t2'] = r[k]['test_fn'].sum()/(r[k]['test_fn'].sum()+r[k]['test_tp'].sum())

max_acc = max([ r[c]['acc'] for c in r ])
min_acc = min([ r[c]['acc'] for c in r ])

fig = plt.figure(figsize=(4, 8))

gs = gridspec.GridSpec(7, 3, wspace=0.0, hspace=0.0, top=1-.5/8, bottom=0.5/8, left=0.5/4, right=1-0.5/4)

for extractor,row in zip(['_', '_Tf_', '_Tf_L1_', '_Tf_L2_', '_Tfidf_', '_Tfidf_L1_', '_Tfidf_L2_'],range(7)):
    for classifier,column in zip(['LogistRegression_L1', 'LogistRegression_L2', 'MultinomialNB'],range(3)):
        cm = r['CountVectorizer'+extractor+classifier]['cm']
        ax = plt.subplot(gs[row,column])
        cmap = mcolors.LinearSegmentedColormap.from_list(extractor+classifier+'map',
            plt.cm.Blues(np.linspace(0,1,256))*r['CountVectorizer'+extractor+classifier]['f'] +
            plt.cm.Reds(np.linspace(0,1,256))*(1-r['CountVectorizer'+extractor+classifier]['f']))

        ax.imshow(cm, interpolation='nearest', cmap=cmap)
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        thresh = cm.max()/2
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, format(cm[i, j], 'd'), horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

plt.subplot(gs[0,0]).set_title('LR L1')
plt.subplot(gs[0,1]).set_title('LR L2')
plt.subplot(gs[0,2]).set_title('MNB')
plt.subplot(gs[0,0]).set_ylabel('No Tf')
plt.subplot(gs[1,0]).set_ylabel('Tf')
plt.subplot(gs[2,0]).set_ylabel('Tf L1')
plt.subplot(gs[3,0]).set_ylabel('Tf L2')
plt.subplot(gs[4,0]).set_ylabel('Tfidf')
plt.subplot(gs[5,0]).set_ylabel('Tfidf L1')
plt.subplot(gs[6,0]).set_ylabel('Tfidf L2')

plt.show()

#TODO: retokenizer should be immutable, haskysh python syntax sucks...
class retokenizer:
    def __init__(self, resubs, stop_words=None, suggest_prob=0.5):
        self.suggest_prob = suggest_prob
        self.resubs = []

        if stop_words is None:
            self.sw = stopwords.words('english')
        else:
            self.sw = stop_words

        for resub in resubs:
            self.resubs.append((re.compile(resub[0]),resub[1]))

    def __deepcopy__(self,memo):
        return self

    def tokenize(self, s):
        for resub in self.resubs:
            s = resub[0].sub(resub[1], s)

        #return [ w for w in ThreadPoolExecutor(max_workers=16).map(word_sel, s.split()) if w not in self.sw ]# with spelling
        return [ w for w in s.split() if w not in stopwords.words('english') ]# without spelling

spamtokenize = retokenizer([
    ('\\.\\.\\.+',' {DOTS} '),
    ('\d\d\+', ' {AGE-RATE} '),
    ('[Xx][Xx]+', ' {TRIPLEX} '),
    ('[:;]-?\)', ' {SMILE} '),
    ('[:;]-?\(', ' {NSMILE} '),
    ('[:;]-?[Pp]', ' {TONGLE} '),
    ('[Ll]+[Oo]+[Ll]+', ' {LOL} '),
    ('[Kk]{3,}', ' {KKK} '),
    ('\d{9,12}', ' {PHONE-NUMBER} '),
    ('0800 \d\d\d \d\d\d\d', ' {0800-PHONE-NUMBER} '),
    ('(£|\\$) *\d+(,\d+|.\d+)?', ' {MONEY} '),
    ('\d*(1 ?st|2 ?nd|3 ?rd|\d ?th)', ' {CARDINAL} '),
    ('\d+ *%', ' {PERCENT} '),
    ('(\w+@\w+\\.\w+(\\.\w+)?)', ' {EMAIL} '),
    ('http(s)? *: */ */', ' {HTTP} '),
    ('([.,!?:;/\\\\\\*\\%\(\)-])', ' \1 '),
    ('"', ' '),#encoding problems
    ('#?&lt;?', ' < '),
    ('#?&gt;?', ' > '),
    ('#?&amp;?', ' & '),
    ('û_', ' '),
    ('û÷', '\''),
    ('å', ' '),
    ('ÌÏ', ' '),
    ('bû÷', ' '),
    ('', ' '),
    ('\\\\x\d\w+', ' '),
    ('÷', ' '),
    ])

def tn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 0]
def fp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 1]
def fn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 0]
def tp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 1]

scoring = {'tp' : make_scorer(tp), 'tn' : make_scorer(tn),
           'fp' : make_scorer(fp), 'fn' : make_scorer(fn)}

base = pd.read_csv("spamþ.csv", encoding='latin1', sep='\x02') # v1: tag, v2: message

if __name__ == '__main__':

    classifiers = {
        'CountVectorizer_LogistRegression_L1':          make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), LogisticRegression(penalty='l1')),
        'CountVectorizer_LogistRegression_L2':          make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), LogisticRegression(penalty='l2')),
        'CountVectorizer_MultinomialNB':                make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), MultinomialNB()),
        'CountVectorizer_Tf_LogistRegression_L1':       make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=False, norm=None), LogisticRegression(penalty='l1')),
        'CountVectorizer_Tf_LogistRegression_L2':       make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=False, norm=None), LogisticRegression(penalty='l2')),
        'CountVectorizer_Tf_MultinomialNB':             make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=False, norm=None), MultinomialNB()),
        'CountVectorizer_Tf_L1_LogistRegression_L1':    make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=False, norm='l1'), LogisticRegression(penalty='l1')),
        'CountVectorizer_Tf_L1_LogistRegression_L2':    make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=False, norm='l1'), LogisticRegression(penalty='l2')),
        'CountVectorizer_Tf_L1_MultinomialNB':          make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=False, norm='l1'), MultinomialNB()),
        'CountVectorizer_Tf_L2_LogistRegression_L1':    make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=False, norm='l2'), LogisticRegression(penalty='l1')),
        'CountVectorizer_Tf_L2_LogistRegression_L2':    make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=False, norm='l2'), LogisticRegression(penalty='l2')),
        'CountVectorizer_Tf_L2_MultinomialNB':          make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=False, norm='l2'), MultinomialNB()),
        'CountVectorizer_Tfidf_LogistRegression_L1':    make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=True, norm=None), LogisticRegression(penalty='l1')),
        'CountVectorizer_Tfidf_LogistRegression_L2':    make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=True, norm=None), LogisticRegression(penalty='l2')),
        'CountVectorizer_Tfidf_MultinomialNB':          make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=True, norm=None), MultinomialNB()),
        'CountVectorizer_Tfidf_L1_LogistRegression_L1': make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=True, norm='l1'), LogisticRegression(penalty='l1')),
        'CountVectorizer_Tfidf_L1_LogistRegression_L2': make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=True, norm='l1'), LogisticRegression(penalty='l2')),
        'CountVectorizer_Tfidf_L1_MultinomialNB':       make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=True, norm='l1'), MultinomialNB()),
        'CountVectorizer_Tfidf_L2_LogistRegression_L1': make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=True, norm='l2'), LogisticRegression(penalty='l1')),
        'CountVectorizer_Tfidf_L2_LogistRegression_L2': make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=True, norm='l2'), LogisticRegression(penalty='l2')),
        'CountVectorizer_Tfidf_L2_MultinomialNB':       make_pipeline(CountVectorizer(strip_accents='unicode', tokenizer=spamtokenize.tokenize), TfidfTransformer(use_idf=True, norm='l2'), MultinomialNB())
    }

    r = {}

    for k, v in classifiers.items():
        r[k] = cross_validate(v, base['v2'], base['v1'], n_jobs=-1, scoring=scoring, cv=StratifiedKFold(n_splits=10, shuffle=True).split(base, base['v1']))

#%store -r r

for k in r.keys():
    r[k]['cm'] = np.array([[r[k]['test_tn'].sum(), r[k]['test_fp'].sum()], [r[k]['test_fn'].sum(), r[k]['test_tp'].sum()]])
    r[k]['prec'] = r[k]['test_tp'].sum()/(r[k]['test_tp'].sum()+r[k]['test_fp'].sum())
    r[k]['acc'] = (r[k]['test_tp'].sum()+r[k]['test_tn'].sum())/(r[k]['test_tp'].sum()+r[k]['test_tn'].sum()+r[k]['test_fn'].sum()+r[k]['test_fp'].sum())
    r[k]['esp'] = r[k]['test_tn'].sum()/(r[k]['test_tn'].sum()+r[k]['test_fp'].sum())
    r[k]['rec'] = r[k]['test_tp'].sum()/(r[k]['test_tp'].sum()+r[k]['test_fn'].sum())
    r[k]['f'] = 2/((1/r[k]['prec'])+(1/r[k]['rec']))
    r[k]['t1'] = r[k]['test_fp'].sum()/(r[k]['test_fp'].sum()+r[k]['test_tn'].sum())
    r[k]['t2'] = r[k]['test_fn'].sum()/(r[k]['test_fn'].sum()+r[k]['test_tp'].sum())

max_acc = max([ r[c]['acc'] for c in r ])
min_acc = min([ r[c]['acc'] for c in r ])

fig = plt.figure(figsize=(4, 8))

gs = gridspec.GridSpec(7, 3, wspace=0.0, hspace=0.0, top=1-.5/8, bottom=0.5/8, left=0.5/4, right=1-0.5/4)

for extractor,row in zip(['_', '_Tf_', '_Tf_L1_', '_Tf_L2_', '_Tfidf_', '_Tfidf_L1_', '_Tfidf_L2_'],range(7)):
    for classifier,column in zip(['LogistRegression_L1', 'LogistRegression_L2', 'MultinomialNB'],range(3)):
        cm = r['CountVectorizer'+extractor+classifier]['cm']
        ax = plt.subplot(gs[row,column])
        cmap = mcolors.LinearSegmentedColormap.from_list(extractor+classifier+'map',
            plt.cm.Blues(np.linspace(0,1,256))*r['CountVectorizer'+extractor+classifier]['f'] +
            plt.cm.Reds(np.linspace(0,1,256))*(1-r['CountVectorizer'+extractor+classifier]['f']))

        ax.imshow(cm, interpolation='nearest', cmap=cmap)
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        thresh = cm.max()/2
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, format(cm[i, j], 'd'), horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

plt.subplot(gs[0,0]).set_title('LR L1')
plt.subplot(gs[0,1]).set_title('LR L2')
plt.subplot(gs[0,2]).set_title('MNB')
plt.subplot(gs[0,0]).set_ylabel('No Tf')
plt.subplot(gs[1,0]).set_ylabel('Tf')
plt.subplot(gs[2,0]).set_ylabel('Tf L1')
plt.subplot(gs[3,0]).set_ylabel('Tf L2')
plt.subplot(gs[4,0]).set_ylabel('Tfidf')
plt.subplot(gs[5,0]).set_ylabel('Tfidf L1')
plt.subplot(gs[6,0]).set_ylabel('Tfidf L2')

plt.show()
